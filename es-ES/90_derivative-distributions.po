# AUTHOR <EMAIL@ADDRESS>, YEAR.
msgid ""
msgstr "Project-Id-Version: 0\nPOT-Creation-Date: 2020-05-17 17:54+0200\nPO-Revision-Date: 2020-06-24 00:42+0000\nLast-Translator: Manuel Alvarez <manuelalvarezgomez@gmail.com>\nLanguage-Team: Spanish <https://hosted.weblate.org/projects/debian-handbook/90_derivative-distributions/es/>\nLanguage: es-ES\nMIME-Version: 1.0\nContent-Type: application/x-publican; charset=UTF-8\nContent-Transfer-Encoding: 8bit\nPlural-Forms: nplurals=2; plural=n != 1;\nX-Generator: Weblate 4.2-dev\n"

msgid "live CD"
msgstr "live CD"

msgid "Specificities"
msgstr "Especificidades"

msgid "Particular Choices"
msgstr "Elecciones particulares"

msgid "Derivative Distributions"
msgstr "Distribuciones derivadas"

msgid "<indexterm><primary>derivative distribution</primary></indexterm> <indexterm><primary>distribution, derivative</primary></indexterm> Many Linux distributions are derivatives of Debian and reuse Debian's package management tools. They all have their own interesting properties, and it is possible one of them will fulfill your needs better than Debian itself."
msgstr "<indexterm><primary>distribución derivada</primary></indexterm> <indexterm><primary>derivada, distribuciones</primary></indexterm> Muchas distribuciones Linux son derivadas de Debian y reutilizan las herramientas de gestión de paquetes de Debian. Todas tienen características interesantes y es posible que una de ellas se adapte mejor a sus necesidades que Debian en sí."

msgid "Census and Cooperation"
msgstr "Censo y cooperación"

msgid "The Debian project fully acknowledges the importance of derivative distributions and actively supports collaboration between all involved parties. This usually involves merging back the improvements initially developed by derivative distributions so that everyone can benefit and long-term maintenance work is reduced."
msgstr "El Proyecto Debian reconoce plenamente la importancia de distribuciones derivadas y respalda activamente la colaboración entre todas las partes involucradas. Usualmente esto involucra integrar mejoras desarrolladas inicialmente por una distribución derivada de tal manera que cualquiera pueda beneficiarse y se reduzca el trabajo de mantenimiento a largo plazo."

msgid "This explains why derivative distributions are invited to become involved in discussions on the <literal>debian-derivatives@lists.debian.org</literal> mailing-list, and to participate in the derivative census. This census aims at collecting information on work happening in a derivative so that official Debian maintainers can better track the state of their package in Debian variants. <ulink type=\"block\" url=\"https://wiki.debian.org/DerivativesFrontDesk\" /><ulink type=\"block\" url=\"https://wiki.debian.org/Derivatives/Census\" />"
msgstr "Esto explica porqué se invita a las distribuciones derivadas a involucrarse en las discusiones en la lista de correo <literal>debian-derivatives@lists.debian.org</literal> y participar en el censo de derivados. Este censo tiene el objetivo de recolectar información sobre el trabajo que ocurre en un derivado para que los desarrolladores Debian oficiales puedan seguir más fácilmente el estado de sus paquetes en las variantes de Debian. <ulink type=\"block\" url=\"https://wiki.debian.org/DerivativesFrontDesk\" /><ulink type=\"block\" url=\"http://wiki.debian.org/Derivatives/Census\" />"

msgid "Let us now briefly describe the most interesting and popular derivative distributions."
msgstr "Ahora describiremos brevemente las distribuciones derivadas más interesantes y populares."

msgid "Ubuntu"
msgstr "Ubuntu"

msgid "<primary>Ubuntu</primary>"
msgstr "<primary>Ubuntu</primary>"

msgid "Ubuntu made quite a splash when it came on the free software scene, and for good reason: Canonical Ltd., the company that created this distribution, started by hiring thirty-odd Debian developers and publicly stating the far-reaching objective of providing a distribution for the general public with a new release twice a year. They also committed to maintaining each version for a year and a half."
msgstr "Ubuntu causó gran revuelo cuando llegó al escenario del software libre, y por buenas razones: Canonical Ltd., la empresa que creó esta distribución, comenzó contratando poco más de treinta desarrolladores Debian y publicando su objetivo a muy largo plazo de proveer una distribución para el público en general con una nueva versión dos veces al año. También se comprometieron a mantener cada versión por un año y medio."

msgid "These objectives necessarily involve a reduction in scope; Ubuntu focuses on a smaller number of packages than Debian, and relies primarily on the GNOME desktop (although there are Ubuntu derivatives that come with other desktop environments). Everything is internationalized and made available in a great many languages."
msgstr "Estos objetivos necesariamente conllevaron una reducción en su alcance; Ubuntu se concentra en una menor cantidad de paquetes que Debian y está basada principalmente en el escritorio GNOME (aunque existen distribuciones derivadas de Ubuntu que incluyen otros entornos de escritorio). Todo es internacionalizado y está disponible en muchos idiomas."

msgid "<primary>Kubuntu</primary>"
msgstr "<primary>Kubuntu</primary>"

msgid "So far, Ubuntu has managed to keep this release rhythm. They also publish <emphasis>Long Term Support</emphasis> (LTS) releases, with a 5-year maintenance promise. As of June 2019, the current LTS version is version 18.04, nicknamed Bionic Beaver. The last non-LTS version is version 19.04, nicknamed Disco Dingo. Version numbers describe the release date: 19.04, for example, was released in April 2019."
msgstr "Hasta ahora, Ubuntu logró mantener este ritmo de publicación. También publican versiones de <emphasis>soporte a largo plazo</emphasis> (LTS: «Long Term Support»), con una promesa de manutención de 5 años. En Junio de 2019, la versión LTS actual es la 18.04, apodada «Bionic Beaver». La última versión no LTS es la 19.04, apodada «Disco Dingo». Los números de versión describen la fecha de publicación: 19.04, por ejemplo, fue publicada en Abril de 2019."

msgid "<emphasis>IN PRACTICE</emphasis> Ubuntu's support and maintenance promise"
msgstr "<emphasis>EN LA PRÁCTICA</emphasis> el soporte que ofrece Ubuntu y la promesa de mantenimiento"

msgid "<primary><literal>main</literal></primary>"
msgstr "<primary><literal>main</literal></primary>"

msgid "<primary><literal>restricted</literal></primary>"
msgstr "<primary><literal>restricted</literal></primary>"

msgid "<primary><literal>universe</literal></primary>"
msgstr "<primary><literal>universe</literal></primary>"

msgid "<primary><literal>multiverse</literal></primary>"
msgstr "<primary><literal>multiverse</literal></primary>"

msgid "Canonical has adjusted multiple times the rules governing the length of the period during which a given release is maintained. Canonical, as a company, promises to provide security updates to all the software available in the <literal>main</literal> and <literal>restricted</literal> sections of the Ubuntu archive, for 5 years for LTS releases and for 9 months for non-LTS releases. Everything else (available in the <literal>universe</literal> and <literal>multiverse</literal>) is maintained on a best-effort basis by volunteers of the MOTU team (<emphasis>Masters Of The Universe</emphasis>). Be prepared to handle security support yourself if you rely on packages of the latter sections."
msgstr "Canonical ha ajustado varias veces las reglas que controlan la longitud del período durante el que se mantiene una publicación dada. Canonical, como empresa, promete proveer actualizaciones de seguridad para todo el software disponible en las secciones <literal>main</literal> y <literal>restricted</literal> del compendio de Ubuntu durante 5 años para publicaciones LTS y por 9 meses para publicaciones que no lo sean. Los miembros del equipo MOTU (<emphasis>dueños del universo</emphasis>: «Masters Of The Universe») mantienen todos los demás paquetes (disponibles en <literal>universe</literal> y <literal>multiverse</literal>) según el mejor esfuerzo posible. Prepárese para gestionar el soporte de seguridad por su cuenta si depende de paquetes en estas últimas secciones."

msgid "Ubuntu has reached a wide audience in the general public. Millions of users were impressed by its ease of installation, and the work that went into making the desktop simpler to use."
msgstr "Ubuntu llegó a una amplia audiencia en el público general. Millones de usuarios se impresionaron por su facilidad de instalación y el trabajo que se realizó en hacer que el escritorio sea más sencillo de utilizar."

msgid "Ubuntu and Debian used to have a tense relationship; Debian developers who had placed great hopes in Ubuntu contributing directly to Debian were disappointed by the difference between the Canonical marketing, which implied Ubuntu were good citizens in the Free Software world, and the actual practice where they simply made public the changes they applied to Debian packages. Things have been getting better over the years, and Ubuntu has now made it general practice to forward patches to the most appropriate place (although this only applies to external software they package and not to the Ubuntu-specific software such as Mir or Unity). <ulink type=\"block\" url=\"https://www.ubuntu.com/\" />"
msgstr "Solía haber una relación tensa entre Ubuntu y Debian; Los desarrolladores de Debian, que pusieron grandes esperanzas en que Ubuntu colaborase directamente con Debian, quedaron defraudados por la diferencia del marketing de Canonical, el cual daba a entender que en Ubuntu se encontraban los buenos ciudadanos del mundo del Software Libre, y la realidad era que simplemente publicaban los cambios que hacían sobre los paquetes de Debian. Las cosas han ido a mejor a lo largo de los años, y Ubuntu ha generalizado la práctica de redirigir los parches al sitio más apropiado (aunque esto solo se aplique al software externo que empaquetan y no al específico de Ubuntu, tales como Mir o Unity). <ulink type=\"block\" url=\"https://www.ubuntu.com/\" />"

msgid "Linux Mint"
msgstr "Linux Mint"

msgid "<primary>Linux Mint</primary>"
msgstr "<primary>Linux Mint</primary>"

msgid "Linux Mint is a (partly) community-maintained distribution, supported by donations and advertisements. Their flagship product is based on Ubuntu, but they also provide a “Linux Mint Debian Edition” variant that evolves continuously (as it is based on Debian Testing). In both cases, the initial installation involves booting a live DVD or a live USB storage device."
msgstr "Linux Mint es una distribución (parcialmente) mantenida por la comunidad, respaldada con donaciones y publicidad. Su producto estrella está basado en Ubuntu, pero también proveen una variante «Linux Mint Debian Edition» que evoluciona continuamente (y está basada en Debian Testing). En ambos casos, la instalación inicial involucra arrancar con un liveDVD o un LiveUSB."

msgid "The distribution aims at simplifying access to advanced technologies, and provides specific graphical user interfaces on top of the usual software. For instance, Linux Mint relies on Cinnamon instead of GNOME by default (but it also includes MATE as well as Xfce); similarly, the package management interface, although based on APT, provides a specific interface with an evaluation of the risk from each package update."
msgstr "La distribución intenta simplificar el acceso a tecnologías avanzadas y provee interfaces gráficas específicas sobre el software usual. Por ejemplo, Linux Mint está basado en Cinnamon en vez de GNOME por defecto (pero también incluye MATE así como también Xfce) provee un sistema de menús diferente; de forma similar, la interfaz de gestión de paquetes, aunque basada en APT, provee una interfaz específica con una evaluación del riesgo en cada actualización de un paquete."

msgid "Linux Mint includes a large amount of proprietary software to improve the experience of users who might need those. For example: Adobe Flash and multimedia codecs. <ulink type=\"block\" url=\"https://linuxmint.com/\" />"
msgstr "Linux Mint incluye una gran cantidad de software privativo para mejorar la experiencia de los usuarios que lo puedan necesitar. Por ejemplo: Adobe Flash y «codecs» multimedia. <ulink type=\"block\" url=\"https://linuxmint.com/\" />"

msgid "Knoppix"
msgstr "Knoppix"

msgid "<primary><foreignphrase>live CD</foreignphrase></primary>"
msgstr "<primary><foreignphrase>live CD</foreignphrase></primary>"

msgid "<primary>Knoppix</primary>"
msgstr "<primary>Knoppix</primary>"

msgid "<primary>bootable CD-ROM</primary>"
msgstr "<primary>arranque, CD-ROM de arranque</primary>"

msgid "<primary>CD-ROM</primary><secondary>bootable</secondary>"
msgstr "<primary>CD-ROM</primary><secondary>de arranque</secondary>"

msgid "The Knoppix distribution barely needs an introduction. It was the first popular distribution to provide a <emphasis>live CD</emphasis>; in other words, a bootable CD-ROM that runs a turn-key Linux system with no requirement for a hard-disk — any system already installed on the machine will be left untouched. Automatic detection of available devices allows this distribution to work in most hardware configurations. The CD-ROM includes almost 2 GB of (compressed) software, and the DVD-ROM version has even more."
msgstr "La distribución Knoppix casi no necesita introducción. Fue la primera distribución popular que proveyó un <emphasis>liveCD</emphasis>; en otras palabras, un CD-ROM de arranque que ejecutaba un sistema Linux listo sin necesitar un disco duro — se mantendría intacto cualquier sistema ya instalado en la máquina. La detección automática de los dispositivos disponibles le permitió a esta distribución funcionar en la mayoría de las configuraciones de hardware. El CD-ROM incluye casi 2 Gb de software (comprimido) y la versión en DVD-ROM todavía contiene más."

msgid "Combining this CD-ROM to a USB stick allows carrying your files with you, and to work on any computer without leaving a trace — remember that the distribution doesn't use the hard-disk at all. Knoppix uses LXDE (a lightweight graphical desktop) by default, but the DVD version also includes GNOME and Plasma. Many other distributions provide other combinations of desktops and software. This is, in part, made possible thanks to the <emphasis role=\"pkg\">live-build</emphasis> Debian package that makes it relatively easy to create a live CD. <ulink type=\"block\" url=\"https://live-team.pages.debian.net/live-manual/\" />"
msgstr "La combinación de este CD-ROM y una llave USB le permite llevar sus archivos a todos lados y trabajar en cualquier equipo sin dejar rastros — recuerde que la distribución no utiliza el disco duro en absoluto. Knoppix utiliza LXDE (un escritorio gráfico liviano) por defecto, pero la versión en DVD incluye también GNOME y Plasma. Muchas otras distribuciones proveen otras combinaciones de escritorios y software. Esto es posible, en parte, gracias al paquete Debian <emphasis role=\"pkg\">live-build</emphasis> que hace relativamente sencillo crear un liveCD. <ulink type=\"block\" url=\"https://live-team.pages.debian.net/live-manual/\" />"

msgid "<primary><emphasis role=\"pkg\">live-build</emphasis></primary>"
msgstr "<primary><emphasis role=\"pkg\">live-build</emphasis></primary>"

msgid "Note that Knoppix also provides an installer: you can first try the distribution as a live CD, then install it on a hard-disk to get better performance. <ulink type=\"block\" url=\"https://www.knopper.net/knoppix/index-en.html\" />"
msgstr "Sepa que Knoppix también provee un instalador: puede primero probar la distribución como live CD y luego instalarla en un disco duro para obtener mejor rendimiento. <ulink type=\"block\" url=\"https://www.knopper.net/knoppix/index-en.html\" />"

msgid "Aptosid and Siduction"
msgstr "Aptosid y Siduction"

msgid "<primary>Sidux</primary>"
msgstr "<primary>Sidux</primary>"

msgid "<primary>Aptosid</primary>"
msgstr "<primary>Aptosid</primary>"

msgid "<primary>Siduction</primary>"
msgstr "<primary>Siduction</primary>"

msgid "These community-based distributions track the changes in Debian <emphasis role=\"distribution\">Sid</emphasis> (<emphasis role=\"distribution\">Unstable</emphasis>) — hence their name. The modifications are limited in scope: the goal is to provide the most recent software and to update drivers for the most recent hardware, while still allowing users to switch back to the official Debian distribution at any time. Aptosid was previously known as Sidux, and Siduction is a more recent fork of Aptosid. <ulink type=\"block\" url=\"http://aptosid.com\" /> <ulink type=\"block\" url=\"https://siduction.org\" />"
msgstr "Esta distribución basada en la comunidad sigue los cambios de Debian <emphasis role=\"distribution\">Sid</emphasis> (<emphasis role=\"distribution\">Unstable</emphasis>) — de allí su nombre — e intenta publicar 4 versiones nuevas cada año. Las modificaciones tienen alcances limitados: el objetivo es proveer el software más reciente y actualizar los controladores para el hardware más reciente al mismo tiempo que permite a sus usuarios volver a la distribución oficial de Debian en cualquier momento. Aptosid era conocido anteriormente como Sidux, y Siduction es una bifurcación más reciente de Aptosid. <ulink type=\"block\" url=\"http://aptosid.com\" /> <ulink type=\"block\" url=\"https://siduction.org\" />"

msgid "Grml"
msgstr "Grml"

msgid "<primary>Grml</primary>"
msgstr "<primary>Grml</primary>"

msgid "Grml is a live CD with many tools for system administrators, dealing with installation, deployment, and system rescue. The live CD is provided in two flavors, <literal>full</literal> and <literal>small</literal>, both available for 32-bit and 64-bit PCs. Obviously, the two flavors differ by the amount of software included and by the resulting size. <ulink type=\"block\" url=\"https://grml.org\" />"
msgstr "Grml es un liveCD con muchas herramientas para administradores de sistemas que tienen que ver con la instalación, despliegue y rescate de sistemas. Se provee el liveCD en dos versiones, <literal>full</literal> («completo») y <literal>small</literal> («pequeño»), ambas disponibles para equipos de 32 y 64 bits. Obviamente, la diferencia entre estas versiones es la cantidad de software incluído y el tamaño del resultado. <ulink type=\"block\" url=\"https://grml.org\" />"

msgid "Tails"
msgstr "Tails"

msgid "<primary>Tails</primary>"
msgstr "<primary>Tails</primary>"

msgid "Tails (The Amnesic Incognito Live System) aims at providing a live system that preserves anonymity and privacy. It takes great care in not leaving any trace on the computer it runs on, and uses the Tor network to connect to the Internet in the most anonymous way possible. <ulink type=\"block\" url=\"https://tails.boum.org\" />"
msgstr "Tails (The Amnesic Incognito Live System, el Sistema vivo incógnito y amnésico) pretende ofrecer un sistema vivo que guarde el anonimato y la privacidad. Se cuida de no dejar ningun rastro en el ordenador donde se ejecuta y usa la red Tor para conectarse a internet de la forma lo más anónima posible. <ulink type=\"block\" url=\"https://tails.boum.org\" />"

msgid "Kali Linux"
msgstr "Kali Linux"

msgid "<primary>Kali</primary>"
msgstr "<primary>Kali</primary>"

msgid "<primary>penetration testing</primary>"
msgstr "<primary>pruebas de penetración</primary>"

msgid "<primary>forensics</primary>"
msgstr "<primary>forense</primary>"

msgid "Kali Linux is a Debian-based distribution specializing in penetration testing (“pentesting” for short). It provides software that helps auditing the security of an existing network or computer while it is live, and analyze it after an attack (which is known as “computer forensics”). <ulink type=\"block\" url=\"https://kali.org\" />"
msgstr "Kali Linux es una distribución derivada de Debian especializada en pruebas de penetración («pentesting» para acortar). Provee software para auditar la seguridad de una red o el equipo en el que se ejecuta, y analiza los resultados después del ataque (lo que es conocido como «informática forense»). <ulink type=\"block\" url=\"https://kali.org\" />"

msgid "Devuan"
msgstr "Devuan"

msgid "<primary>Devuan</primary>"
msgstr "<primary>Devuan</primary>"

msgid "Devuan is a fork of Debian started in 2014 as a reaction to the decision made by Debian to switch to <command>systemd</command> as the default init system. A group of users attached to <command>sysv</command> and opposing drawbacks to <command>systemd</command> started Devuan with the objective of maintaining a <command>systemd</command>-less system. <ulink type=\"block\" url=\"https://devuan.org\" />"
msgstr "Devuan es una bifurcación de Debian: empezó en 2014 como la reacción a la decisión tomada por Debian de cambiar a <command>systemd</command> como sistema de inicio por defecto. Un grupo de usuarios vinculados por <command>sysv</command> y contrarios a los inconvenientes que proporciona <command>systemd</command> empezaron Devuan con el objetivo de mantener un sistema libre de <command>systemd</command>. <ulink type=\"block\" url=\"http://devuan.org\" />"

msgid "DoudouLinux"
msgstr "DoudouLinux"

msgid "<primary>DoudouLinux</primary>"
msgstr "<primary>DoudouLinux</primary>"

msgid "DoudouLinux targets young children (starting from 2 years old). To achieve this goal, it provides a heavily customized graphical interface (based on LXDE) and comes with many games and educative applications. Internet access is filtered to prevent children from visiting problematic websites. Advertisements are blocked. The goal is that parents should be free to let their children use their computer once booted into DoudouLinux. And children should love using DoudouLinux, just like they enjoy their gaming console. <ulink type=\"block\" url=\"https://www.doudoulinux.org\" />"
msgstr "DoudouLinux tiene como objetivo a los niños pequeños (desde los 2 años de edad). Para conseguir este objetivo, provee una interfaz gráfica muy modificada (basada en LXDE) y contiene muchos juegos y aplicaciones educativas. El acceso a Internet está filtrado para evitar que los niños accedan a sitios web problemáticos. Y la publicidad se encuentra bloqueada. El objetivo es que los padres puedan dejar tranquilamente que sus hijos utilicen el equipo una vez que inició DoudouLinux. Y los niños deberían estar encantados con DoudouLinux de la misma forma que disfrutan consolas de videojuegos. <ulink type=\"block\" url=\"https://www.doudoulinux.org\" />"

msgid "Raspbian"
msgstr "Raspbian"

msgid "<primary>Raspbian</primary>"
msgstr "<primary>Raspbian</primary>"

msgid "<primary>Raspberry Pi</primary>"
msgstr "<primary>Raspberry Pi</primary>"

msgid "Raspbian is a rebuild of Debian optimized for the popular (and inexpensive) Raspberry Pi family of single-board computers. The hardware for that platform is more powerful than what the Debian <emphasis role=\"architecture\">armel</emphasis> architecture can take advantage of, but lacks some features that would be required for <emphasis role=\"architecture\">armhf</emphasis>; so Raspbian is a kind of intermediary, rebuilt specifically for that hardware and including patches targeting this computer only. <ulink type=\"block\" url=\"https://raspbian.org\" />"
msgstr "Raspbian es una reconstrucción de Debian optimizada para la popular (y económica) familia Raspberry Pi de ordenadores en placa. El hardware para esta plataforma es más potente que la arquitectura Debian <emphasis role=\"architecture\">armel</emphasis> puede ofrecer y que se requeriría para <emphasis role=\"architecture\">armhf</emphasis>; así que Raspbian es una clase de intermediario, reconstruido específicamente para este hardware e incluye parches enfocados a solo a este ordenador. <ulink type=\"block\" url=\"https://raspbian.org\" />"

msgid "PureOS"
msgstr "PureOS"

msgid "<primary>PureOS</primary>"
msgstr "<primary>PureOS</primary>"

msgid "<primary>Purism</primary>"
msgstr "<primary>Purism</primary>"

msgid "PureOS is a Debian-based distribution focused on privacy, convenience and security. It follows the <ulink url=\"https://www.gnu.org/distros/free-system-distribution-guidelines.html\">GNU Free System Distribution Guidelines</ulink>, used by the Free Software Foundation to qualify a distribution as free. The social purpose company Purism guides its development. <ulink type=\"block\" url=\"https://pureos.net/\" />"
msgstr "PureOS es una distribución basada en Debian centrada en la privacidad, la facilidad y la seguridad. Sigue las <ulink url=\"https://www.gnu.org/distros/free-system-distribution-guidelines.html\">Pautas para distribuciones de sistema libres (GNU FSDG)</ulink>, utilizada por la FSF para calificar una distribución como libre. La empresa de propósito social Purism guía su desarrollo. <ulink type=\"block\" url=\"https://pureos.net/\" />"

msgid "SteamOS"
msgstr "SteamOS"

msgid "<primary>SteamOS</primary>"
msgstr "<primary>SteamOS</primary>"

msgid "<primary>Valve Corporation</primary>"
msgstr "<primary>Valve Corporation</primary>"

msgid "SteamOS is a gaming-oriented Debian-based distribution developed by Valve Corporation. It is used in the Steam Machine, a line of gaming computers. <ulink type=\"block\" url=\"https://store.steampowered.com/steamos/\" />"
msgstr "SteamOS es una distribución basada en Debian orientada a los juegos desarrollada por Valve Corporation. Es usada en la Steam Machine, una línea de computadoras para juegos. <ulink type=\"block\" url=\"https://store.steampowered.com/steamos/\" />"

msgid "And Many More"
msgstr "Y muchas más"

msgid "<primary>Distrowatch</primary>"
msgstr "<primary>Distrowatch</primary>"

msgid "The Distrowatch website references a huge number of Linux distributions, many of which are based on Debian. Browsing this site is a great way to get a sense of the diversity in the free software world. <ulink type=\"block\" url=\"https://distrowatch.com\" />"
msgstr "El sitio web Distrowatch hace referencia a una inmensa cantidad de distribuciones Linux, muchas de las cuales están basadas en Debian. Navegar este sitio es una excelente forma de adentrarse en la diversidad del mundo del software libre. <ulink type=\"block\" url=\"https://distrowatch.com\" />"

msgid "The search form can help track down a distribution based on its ancestry. In June 2019, selecting Debian led to 127 active distributions! <ulink type=\"block\" url=\"https://distrowatch.com/search.php\" />"
msgstr "El formulario de búsqueda le puede ayudar a rastrear una distribución según su linaje. En Junio de 2019, ¡seleccionar Debian llevaba a 127 distribuciones activas! <ulink type=\"block\" url=\"https://distrowatch.com/search.php\" />"

#~ msgid "Tanglu"
#~ msgstr "Tanglu"

#~ msgid "<primary>Tanglu</primary>"
#~ msgstr "<primary>Tanglu</primary>"

#~ msgid "Tanglu is another Debian derivative; this one is based on a mix of Debian <emphasis role=\"distribution\">Testing</emphasis> and <emphasis role=\"distribution\">Unstable</emphasis>, with patches to some packages. Its goal is to provide a modern desktop-friendly distribution based on fresh software, without the release constraints of Debian. <ulink type=\"block\" url=\"http://tanglu.org\" />"
#~ msgstr "Tanglu es otra derivada de Debian; esta se basa en una mezcla de Debian <emphasis role=\"distribution\">Testing</emphasis> y <emphasis role=\"distribution\">Unstable</emphasis>. Su meta es ofrecer un entorno de escritorio amigable y moderno basado en software reciente, sin los acarreos de liberación de Debian. <ulink type=\"block\" url=\"http://tanglu.org\" />"

#~ msgid "This interaction is becoming more common over time, thanks in part to the Ubuntu community and the efforts it makes in educating its new contributors. <ulink type=\"block\" url=\"http://www.ubuntu.com/\" />"
#~ msgstr "Esta interacción es cada vez más común con el tiempo, en parte gracias a la comunidad de Ubuntu y el esfuerzo que realiza en la educación de sus nuevos colaboradores. <ulink type=\"block\" url=\"http://www.ubuntu.com/\" />"

#~ msgid "SimplyMEPIS"
#~ msgstr "SimplyMEPIS"

#~ msgid "<primary>SimplyMEPIS</primary>"
#~ msgstr "<primary>SimplyMEPIS</primary>"

#~ msgid "SimplyMEPIS is a commercial distribution very similar to Knoppix. It provides a turn-key Linux system from a LiveCD, and includes a number of non-free software packages: device drivers for nVidia video cards, Flash for animations embedded in many websites, RealPlayer, Sun's Java, and so on. The goal is to provide a 100 % working system out of the box. Mepis is internationalized and handles many languages. <ulink type=\"block\" url=\"http://www.mepis.org/\" />"
#~ msgstr "SimplyMEPIS es una distribución comercial muy similar a Knoppix. Provee un sistema Linux listo desde un LiveCD e incluye una cantidad de paquetes de software privativo: controladores de dispositivo para tarjetas de video nVidia, Flash para las animaciones incluidas en muchos sitios web, RealPlayer, Java de Sun, etc. El objetivo es proveer un sistema 100% funcional desde el primer momento. Mepis está internacionalizado e incluye muchos idiomas. <ulink type=\"block\" url=\"http://www.mepis.org/\" />"

#~ msgid "This distribution was originally based on Debian; it went to Ubuntu for a while, then came back to Debian Stable, which allows its developers to focus on adding features without having to stabilize packages coming from Debian's <emphasis role=\"distribution\">Unstable</emphasis> distribution."
#~ msgstr "Esta distribución estaba originalmente basada en Debian; se mudó a Ubuntu por un tiempo, luego volvió a Debian Stable, lo que le permitía a sus desarrolladores agregar funcionalidad sin tener que estabilizar los paquetes que provenían de la distribución <emphasis role=\"distribution\">Unstable</emphasis> de Debian."

#~ msgid "This interaction is becoming more common over time, thanks in part to the Ubuntu community and the efforts it makes in educating its new contributors. But this policy is still not enforced by Canonical on its employees. Some kept true to their roots, and do make the required effort (Colin Watson, Martin Pitt and Matthias Klose are noteworthy in this regard), but others — often overworked — can no longer find it in them. <ulink type=\"block\" url=\"http://www.ubuntu.com/\" />"
#~ msgstr "Esta interacción es cada vez más común, gracias en parte a la comunidad de Ubuntu y los efuerzos que realiza educando sus nuevos contribuyentes. Pero Canonical todavía no fuerza esta política en sus empleados. Algunos se mantienen fieles a sus raíces y realizan el esfuerzo necesario (Colin Watson, Martin Pitt y Matthias Klose son notables en este aspecto), pero otros — generalmente con demasiado trabajo — ya no lo hacen. <ulink type=\"block\" url=\"http://www.ubuntu.com/\" />"

#~ msgid "This distribution provides a tiny LiveCD, weighing only 50 MB, so that it can fit on a CD-ROM with the shape and size of a business card. To achieve this, Damn Small Linux only includes lightweight software tools. This can be interesting to use a Debian-like system on an aging computer. <ulink type=\"block\" url=\"http://www.damnsmalllinux.org/\" />"
#~ msgstr "Esta distribución provee un LiveCD diminuto, pesa sólo 50 MB por lo que entra en un CD-ROM de la forma y tamaño de una tarjeta de negocios. Para conseguirlo, Damn Small Linux sólo incluye herramientas de software livianas. Esto puede ser interesante para utilizar un sistema similar a Debian en un equipo antiguo. <ulink type=\"block\" url=\"http://www.damnsmalllinux.org/\" />"
